<?php

session_start();

//Inicializamos las variables.

$usuario= "";
$email="";

$errors = array();    //Array de almacenamiento de errores.

//Conexion a la base de datos.
$db=mysqli_connect('192.168.1.108','root','example','tucine_2') or die("No se puede conectar a la base de datos");

//localhost: dirección ip de la base de datos --> en este caso local.
//root: el user con el que vamos a hacer login.
//tucine_prueba ->nombre base de datos.
if (isset($_POST['registrar'])){

//Obtenemos los valores de los campos de registro.
$usuario = mysqli_real_escape_string($db,$_POST['usuario']);
$email = mysqli_real_escape_string($db,$_POST['email']);
$contraseña_1 = mysqli_real_escape_string($db,$_POST['contraseña_1']);
$contraseña_2 = mysqli_real_escape_string($db,$_POST['contraseña_2']);

//Validación de los formularios.

if(empty($usuario)) {
	array_push($errors,"Se requiere nombre de usuario");
}
if(empty($email)) {
	array_push($errors,"Se requiere email");
}
if(empty($contraseña_1)) {
	array_push($errors,"Se requiere contraseña");
}

if ($contraseña_1!=$contraseña_2){
	array_push($errors,"Las contraseñas no coinciden");
}

//Comprobar base de datos por si existe un usuario con el mismo nombre.
//LIMIT=1 basta con encontrar una coincidencia.
//Seleccionamos todos los campos "usuario" de la base de datos donde el nombre de usuario sea igual a nuestro $usuario.
$chequeo_usuario="SELECT * FROM user WHERE username='$usuario' or email= '$email' LIMIT 1";

$resultado=mysqli_query($db, $chequeo_usuario); //Muestra los resultados.
$user=mysqli_fetch_assoc($resultado); //Recupera la fila de resultados.


if($user){   //En caso de que encuentre, es decir, que $user sea "true".

	if ($user['username']==$usuario){
		array_push($errors, "El nombre de usuario ya existe");
	}
	if ($user['email']==$email){
		array_push($errors, "El email está asociado a una cuenta ya existente");
	}
}

//Si pasa todo esto, no ha habido error, entonces lo registramos.

if (count($errors)==0){
	$contraseña=md5($contraseña_1);  //Antes de almacenar la contraseña la encriptamos.
	$query="INSERT INTO user (username, email, password) VALUES ('$usuario', '$email', '$contraseña')";

	mysqli_query($db,$query);
	$_SESSION['username']=$username;
	$_SESSION['success']="Estas dentro";


	header('location: index.php');

}

}

//LOGIN USUARIO

if (isset($_POST['login'])){

	$usuario=mysqli_real_escape_string($db, $_POST['usuario']);
	$contraseña=mysqli_real_escape_string($db, $_POST['contraseña']);

	if (empty($usuario)){
		array_push($errors, "Se requiere nombre de usuario");
	}
	if (empty($contraseña)){
		array_push($errors, "Se requiere contraseña");
	}
	if (count($errors)==0){
		$contraseña=md5($contraseña);

		$query= "SELECT * FROM user WHERE username='$usuario' AND password='$contraseña' ";
		$results=mysqli_query($db,$query);

		if (mysqli_num_rows($results)){
			$_SESSION['username']=$usuario;
			$_SESSION['success']="Acceso correcto";
			header('location: index.php');
		}else{
			array_push($errors,"Usuario o contraseña incorrectos");

		}

	}

	
}

?>