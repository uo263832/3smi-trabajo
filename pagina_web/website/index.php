<!--<?php

/*session_start();

if(isset($_GET['logout'])){
    session_destroy();
    unset($_SESSION['username']);
    header("location : index.php");

}*/


?>   -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  		<title>TUCINE</title>
<!-- 

Highway Template

https://templatemo.com/tm-520-highway

-->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/light-box.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Kanit:100,200,300,400,500,600,700,800,900" rel="stylesheet">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="icon" type="image/png" href="img/LOGO_TUCINE.png" />
    </head>

<body>

    <nav>
        <div class="logo">
            <!--<?php /*if(isset($_SESSION['username'])) : ?>
            <a> Bienvenido <strong> <?php echo $_SESSION['username']; ?> </strong></a>
            
                <button><a href="index.php?logout='1'">logout</a></button>
            
        <?php endif */?>-->
            <a href="index.php">TU<em>CINE</em></a>
             
        </div>
        <div class="menu-icon">
        <span></span>
      </div>
    </nav>

    <div id="video-container">
        <div class="video-overlay"></div>
        <div class="video-content">
            <div class="inner">
              <h1>BIENVENIDO A TU PORTAL DE CINE <em> </em></h1>
              <p>Descubre los últimos estrenos</p>
              
                <div class="scroll-icon">
                    <a class="scrollTo" data-scrollTo="portfolio" href="#"><img src="img/scroll-icon.png" alt=""></a>
                </div>    
            </div>
        </div>
        <div >
			<img class="TUCINE_LOGO" src="img/LOGO_TUCINE.png" >
		</div> 
        <!--<video controls autoplay>
        	<source src="MI.mp4" type="video/mp4">
        </video> --> 
    	</div>
    </div>


    <div class="full-screen-portfolio" id="portfolio">
        <div class="container-fluid">
            <?php include 'visualizar.php' ?>

           <!-- <div class="col-md-4 col-sm-6">
                <div class="portfolio-item">
                    <a href="img/007_ESP.mp4" ><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1>NO TIME <em>TO DIE</em></h1>
                                <p>Cary Fukunaga</p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="img/007.png">
                        </div>
                    </div></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="portfolio-item">
                    <a href="img/BB.mp4"><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1>BAD BOYS<em>FOR LIFE</em></h1>
                                <p>Billal Fallah, Adil El Arbi</p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="img/BB.png">
                        </div>
                    </div></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="portfolio-item">
                	
                    <a href="img/irlandes.mp4"><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1>EL <em> IRLANDES</em></h1>
                                <p>Martin Scorsese</p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="img/irlandes.png">
                        </div>
                    </div></a>
                </div>
            </div>
           <div class="col-md-4 col-sm-6">
                <div class="portfolio-item">
                    <a href="" data-lightbox="image-1"><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1><em></em></h1>
                                <p></p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="">
                        </div>
                    </div></a>
                </div>
            </div> 
             <div class="col-md-4 col-sm-6">
                <div class="portfolio-item">
                    <a href="" data-lightbox="image-1"><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1> <em></em></h1>
                                <p></p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="">
                        </div>
                    </div></a>
                </div>
            </div> 
            <div class="col-md-4 col-sm-6">
                <div class="portfolio-item">
                    <a href="img/holly.mp4"><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1>ONCE UPON A TIME<em>IN HOLLYWOOD</em></h1>
                                <p>Quentin Tarantino</p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="img/holly.png">
                        </div>
                    </div></a>
                </div>
            </div>
               <div class="col-md-4 col-sm-6">
                <div class="portfolio-item">
                    <a href="img/" data-lightbox="image-1"><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1>Thundercats <em>santo</em></h1>
                                <p>Awesome Subtittle Goes Here</p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="img/">
                        </div>
                    </div></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="portfolio-item">
                    <a href="img/" data-lightbox="image-1"><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1>wayfarers <em>yuccie</em></h1>
                                <p>Awesome Subtittle Goes Here</p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="img/">
                        </div>
                    </div></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
            	<div class="portfolio-item">
                	<a href="img/" data-lightbox="image-1"><div class="thumb">
                        <div class="hover-effect">
                            <div class="hover-content">
                                <h1>disrupt <em>street</em></h1>
                                <p>Awesome Subtittle Goes Here</p>
                            </div>
                        </div>
                        <div class="image">
                            <img src="img/">
                        </div>
                    </div></a> 
                </div>
            </div> -->
        </div>
    </div>


    <footer>
        <div class="container-fluid">
            <div class="col-md-12">
                <p>Copyright &copy; 2020 TUCINE 
    
    | Designed by 2020_SMI-1</p>
            </div>
        </div>
    </footer>


      <!-- Modal button -->
    <div class="popup-icon">
      <button id="modBtn" class="modal-btn"><img src="img/contact-icon.png" alt=""></button>
    </div>  

    <!-- Modal -->
    <div id="modal" class="modal">
      <!-- Modal Content -->
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h3 class="header-title">Envia tu comentario a <em>TUCINE</em></h3>
          <div class="close-btn"><img src="img/close_contact.png" alt=""></div>    
        </div>
        <!-- Modal Body -->
        
        <div class="modal-body">
          <div class="col-md-6 col-md-offset-3">
            <form id="contact" autocomplete="off" action="email.php" method="post">
                <div class="row">
                    <div class="col-md-12">
                      <fieldset>
                        <input name="name" type="text" class="form-control" id="name" placeholder="Nombre..." required="">
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <input name="email" type="email" class="form-control" id="email" placeholder="E-mail..." required="">
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <textarea name="message" rows="6" class="form-control" id="message" placeholder="Mensaje..." required=""></textarea>
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <button type="submit" name="envia" id="form-submit" class="btn">Envia</button>
                      </fieldset>
                    </div>
                </div>
            </form>
            </div>
        </div>
      </div>
    </div>

    

    <section class="overlay-menu">
      <div class="container">
        <div class="row">
          <div class="main-menu">
              <ul>
                  <li>
                      <a href="index.php">Inicio</a>
                  </li>
                  <li>
                      <a href="">Búsqueda</a>
                  </li>
                  <li>
                      <a href="subirvideo.php">Sube tu vídeo</a>
                  </li>
                  <li>
                      <a href="login.php">LogIn</a>
                  </li>
                  <li>
                      <a href="">RRSS</a>
                  </li>
                  <!--<li>
                      <a href="single-post.html">Ayuda</a>
                  </li> -->
              </ul>
              <p></p>
          </div>
        </div>
      </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>
    
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    
</body>
</html>